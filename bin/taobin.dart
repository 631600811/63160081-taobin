import 'dart:io';

class Taobin {
  int money = 0;
  int input = 0;
  int select = 0;
  int addmoney = 0;
  List<String> menu = [];
  List<int> price = [];

  showWelcome() {
    print('Welcome to Taobin');
    print('1.Coffee');
    print('2.Tea');
    print('3.Milk');
    print('4.Juice');
    print('Please Select a Menu :');
    input = int.parse(stdin.readLineSync()!);
  }

  showMenu() {
    print('Please Select a Menu :');
    if (input == 1) {
      print('1.Latte');
      print('2.Americano');
      print('3.Capuchino');
      print('4.Espresso');
      menu.add('Latte');
      menu.add('Americano');
      menu.add('Capuchino');
      menu.add('Espresso');
      price.add(35);
      price.add(40);
      price.add(45);
      price.add(40);
    } else if (input == 2) {
      print('1.Green Tea');
      print('2.Lemon Tea');
      print('3.Black Tea');
      print('4.Rose Tea');
      menu.add('Green Tea');
      menu.add('Lemon Tea');
      menu.add('Black Tea');
      menu.add('Rose Tea');
      price.add(40);
      price.add(35);
      price.add(45);
      price.add(50);
    } else if (input == 3) {
      print('1.Chocolate Milk');
      print('2.Strawberry Milk');
      print('3.Almond Milk');
      print('4.Fresh Milk');
      menu.add('Chocolate Milk');
      menu.add('Strawberry Milk');
      menu.add('Almond Milk');
      menu.add('Fresh Milk');
      price.add(40);
      price.add(45);
      price.add(50);
      price.add(35);
    } else if (input == 4) {
      print('1.Lemonade');
      print('2.Orange Juice');
      print('3.Grape Juice');
      print('4.Watermelon Juice');
      menu.add('Lemonade');
      menu.add('Orange Juice');
      menu.add('Grape Juice');
      menu.add('Watermelon Juice');
      price.add(35);
      price.add(30);
      price.add(40);
      price.add(45);
    }
    print('0.Back');
    select = int.parse(stdin.readLineSync()!);
    if (select == 0) {
      showWelcome();
    }
  }

  payment() {
    if (money - price[select - 1] > 0) {
      print('Thank You!!!');
      print('Exchange = ' + (money - price[select - 1]).toString() + ' Bath');
    } else if (money - price[select - 1] == 0) {
      print('Thank You!!!');
    } else {
      print('Please insert more money :');
      addmoney = int.parse(stdin.readLineSync()!);
      money = money + addmoney;
      payment();
    }
  }

  void ConfirmOrder() {
    if (select <= 4 && select >= 1) {
      print('menu' + ' ' + menu[select - 1].toString());
      print('price' + ' ' + price[select - 1].toString() + ' Bath');
      print('Please insert money:');
      money = int.parse(stdin.readLineSync()!);
    }
  }
}
